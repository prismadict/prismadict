#include "util.h"

#include <QString>
#include <QtDebug>

namespace PrismaDict {

Util::Util()
{
}

QString Util::removeDiacritics(QString word)
{
    QString wordNormalized = word.normalized(QString::NormalizationForm_D);
    //todo optimize
    if (wordNormalized != word) {
        QString tmp;
        for (int i = 0;i<wordNormalized.size();i++) {
//            qDebug() << "char cat" << wordNormalized.at(i).category();
            if (wordNormalized[i].category() != QChar::Mark_NonSpacing) {
                tmp += wordNormalized[i];
            }
        }
        wordNormalized = tmp;
        qDebug() << word << " => " << wordNormalized;
    }
    return wordNormalized;
}

bool Util::containsDiacritic(QString word)
{
    return word != word.normalized(QString::NormalizationForm_D);
}

} // namespace PrismaDict
