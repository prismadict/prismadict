#include "wordlistmodel.h"

#include "worddefinition.h"

#include <QHash>
#include <QTextStream>
#include <QStringBuilder>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QDataStream>

#include <QtDebug>

namespace PrismaDict {

WordListModel* WordListModel::loadFromFile(QFile& inputFile)
{
    WordListModel* model = new WordListModel();
    QTextStream errorStream(stderr);
    if (!inputFile.open(QIODevice::ReadOnly)) {
        errorStream << tr("Failed to open file %1.\n").arg(inputFile.fileName());
        return model;
    }
    QDataStream dataStream(&inputFile);
    quint8 byte1=0, byte2=0, byte3=0, byte4=0;
    bool found = false;
    while (!dataStream.atEnd() && !found) {
        byte1 = byte2;
        byte2 = byte3;
        byte3 = byte4;
        dataStream >> byte4;
        if ((quint32)byte4 + (byte3 << 8) + (byte2 << 16) + (byte1 << 24) == (quint32)0x3f786d6c) found = true;
    }
    if (!found) {
        errorStream << "xml start not found";
        return model;
    }

    QTextStream stdoutStream(stdout);
    qint64 pos = inputFile.pos() - 5;
    stdoutStream << "xml pos = " << pos;
    inputFile.seek(pos);

    QXmlStreamReader reader(&inputFile);

    QSet<QString> encounteredElements;
    QString word;
    QString explanation;
    while (!reader.atEnd())
    {
        QXmlStreamReader::TokenType token = reader.readNext();
//        qDebug() << reader.name();
        if(token == QXmlStreamReader::StartElement) {
            if(reader.name() == "iosunit") {
                reader.readNext();
                while(!(reader.tokenType() == QXmlStreamReader::EndElement &&
                        reader.name() == "iosunit") && !reader.atEnd()) {
//                    qDebug() << reader.name();
                    if(reader.tokenType() == QXmlStreamReader::StartElement) {
                        if (reader.name() == "unitname") {
                            word = reader.readElementText(QXmlStreamReader::IncludeChildElements);
//                            qDebug() << "new word" << word;
                            explanation = "";
                        } else {
                            encounteredElements << reader.name().toString();
                            explanation.append(readElementTextVerbatim(&reader, encounteredElements));
//                            explanation.append(reader.readElementText(QXmlStreamReader::IncludeChildElements));
                        }
                    }
                    reader.readNext();
                }
                model->wordList.append(new WordDefinition(word, explanation));
                continue;
            }
        }
    }
        if (reader.error())
        {
            errorStream << tr(
                    "Error: %1 in file %2 at line %3, column %4.\n").arg(
                            reader.errorString(), inputFile.fileName(),
                            QString::number(reader.lineNumber()),
                            QString::number(reader.columnNumber()));
        }
        reader.clear();
        qDebug() << "encountered elems" << encounteredElements.toList();
        return model;
}

QString WordListModel::readElementTextVerbatim(QXmlStreamReader *reader, QSet<QString>& encounteredElements)
{
    QString buf;
    buf = buf % "<" % reader->name().toString() % ">";
    int level = 1;

    while(level > 0 && !reader->atEnd()) {
        reader->readNext();
        switch(reader->tokenType()) {
        case QXmlStreamReader::StartElement:
            buf = buf % "<" % reader->name().toString() % ">";
            encounteredElements << reader->name().toString();
            ++level;
            break;
        case QXmlStreamReader::EndElement:
            buf = buf % "</" % reader->name().toString() % ">";
            --level;
            break;
        case QXmlStreamReader::Characters:
            buf = buf % reader->text();
            break;
        default:
            qDebug() << "woop woop";
        }
    }
    return buf;
}

WordListModel::WordListModel(QObject *parent) :
    QAbstractListModel(parent), fullTextSearch(false)
{
}

int WordListModel::rowCount ( const QModelIndex & parent ) const
{
    Q_UNUSED(parent);
    return wordList.size();
}

QVariant WordListModel::data ( const QModelIndex & index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() >= wordList.size())
        return QVariant();

    WordDefinition* def = wordList.at(index.row());
    switch(role) {
    case Qt::DisplayRole:
        return def->word;
    case WORDDEF_ROLE:
        return QVariant::fromValue<WordDefinition*>(def);
    case FILTER_ROLE:
        return def->wordNormalized;
    case FILTER_FULLTEXT_ROLE:
        return def->explanation;
    default:
        return QVariant();
    }
}

} // namespace PrismaDict
