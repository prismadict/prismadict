#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "aboutwidget.h"

#include "config.h"
#include "util.h"
#include "wordlistmodel.h"
#include "worddefinition.h"

#include <QtCore/QTime>
#include <QFileInfo>
#include <QSortFilterProxyModel>
#include <QtDebug>
#include <QFileDialog>

namespace PrismaDict {

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    aboutDialog(0)
{
    config = new Config();
    ui->setupUi(this);
    init();
}

MainWindow::~MainWindow()
{
    config->windowGeom = saveGeometry();
    delete config;
    delete ui;
}

void MainWindow::init()
{
    filterModel = new QSortFilterProxyModel(this);
    filterModel->setFilterRole(FILTER_ROLE);
    filterModel->setFilterCaseSensitivity(Qt::CaseInsensitive);
    ui->wordListView->setModel(filterModel);

    connect(ui->actionAbout, SIGNAL(triggered()), this, SLOT(onActionAbout_triggered()));

    connect(ui->wordQueryLE, SIGNAL(textChanged(QString)), this, SLOT(searchIncremental(QString)));
    connect(ui->wordListView, SIGNAL(activated(QModelIndex)), this, SLOT(showDefinition(QModelIndex)));
    showDefTimer.setSingleShot(true);
    showDefTimer.setInterval(config->showDefTimeout);
    connect(&showDefTimer, SIGNAL(timeout()), this, SLOT(showFirstDef()));

    connect(ui->explSearchButton, SIGNAL(clicked()), this, SLOT(onExplSearchButton_Clicked()));
    connect(ui->explQueryLE, SIGNAL(returnPressed()), this, SLOT(onExplSearchButton_Clicked()));

    connect(ui->actionReload_style, SIGNAL(triggered()), this, SLOT(reloadStyle()));
    connect(ui->actionOpen_dictionary, SIGNAL(triggered()), this, SLOT(onActionOpen_triggered()));

#ifdef USE_WEBKIT
    if (config->useWebkit) {
        delete ui->meaningTE;
        webview = new QWebView(ui->splitter);
        webview->setObjectName(QString::fromUtf8("webview"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(1);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(webview->sizePolicy().hasHeightForWidth());
        webview->setSizePolicy(sizePolicy);
        ui->splitter->addWidget(webview);
        webview->setHtml("");
    }
#endif
    restoreGeometry(config->windowGeom);
}

void MainWindow::loadDict(QString filename)
{
    qDebug() << "MainWindow::loadDict: " << filename;
    QFile file(filename);
    QTime timer;
    timer.start();
    wordListModel = WordListModel::loadFromFile(file);
    qDebug() << "loaded dict in " << timer.elapsed() << "ms";
    setWindowTitle("PrismaDict - " + QFileInfo(filename).fileName());
    filterModel->setSourceModel(wordListModel);
    ui->statusBar->showMessage(tr("%n word definitions loaded.", "", wordListModel->rowCount()));
}

void MainWindow::loadStyle(QString styleFilename)
{
    qDebug() << "MainWindow::loadStyle: " << styleFilename;
    QFile file(styleFilename);

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug() << "style not found" << styleFilename;
        return;
    }

    QTextStream in(&file);
    style = in.readAll();
}

void MainWindow::reloadStyle()
{
    loadStyle(config->styleFilename);
    ui->wordListView->clearSelection();
}

void MainWindow::onExplSearchButton_Clicked()
{
    ui->wordQueryLE->clear();
    searchFullText(ui->explQueryLE->text());
}

void MainWindow::onActionAbout_triggered() {
    if (aboutDialog == 0) {
        aboutDialog = new AboutWidget(this);
        aboutDialog->setVersion(qApp->applicationVersion());
        aboutDialog->adjustSize();
    }
    aboutDialog->exec();
}

void MainWindow::onActionOpen_triggered() {
    QString filename = QFileDialog::getOpenFileName(this, tr("Select Prisma dictionary executable"), "", tr("Prisma executables (*.exe);;All files (*.*)"));
    if (filename.isNull()) return;
    loadDict(filename);
}

void MainWindow::searchIncremental(QString text)
{
    filterModel->setFilterRole(FILTER_ROLE);
    filterList(text);
}

void MainWindow::searchFullText(QString text)
{
    filterModel->setFilterRole(FILTER_FULLTEXT_ROLE);
    filterList(text);
}

void MainWindow::filterList(QString text)
{
    filterModel->setFilterFixedString(Util::removeDiacritics(text));
    ui->statusBar->showMessage(tr("%n matches.", "", filterModel->rowCount()));
    showDefTimer.start();
}

void MainWindow::showDefinition(QModelIndex proxyIndex)
{
    QModelIndex srcIndex = filterModel->mapToSource(proxyIndex);
    QString html;
    if (srcIndex.isValid()) {
        WordDefinition* def = wordListModel->data(srcIndex, Qt::UserRole).value<WordDefinition*>();
        html = "<html><head><style type=\"text/css\">" + style + "</style></head><body><h3>" + def->word + "</h3>"
                       + def->getPrettyExplanation() + "</body></html>";
        qDebug() << "plain = " << def->explanation;
    } else {
        html = "";
    }
    qDebug() << "html = " << html;
#ifdef USE_WEBKIT
    if (config->useWebkit) {
        webview->setHtml(html);
    } else {
        ui->meaningTE->setHtml(html);
    }
#else
    ui->meaningTE->setHtml(html);
#endif
}

void MainWindow::showFirstDef()
{
    QModelIndex i;
    if (filterModel->filterRole() == FILTER_ROLE) {
        QString word = ui->wordQueryLE->text();
        QModelIndexList il;
        if (Util::containsDiacritic(word)) {
            il = filterModel->match(filterModel->index(0,0), Qt::DisplayRole, word);
        } else {
            il = filterModel->match(filterModel->index(0,0), FILTER_ROLE, word);
        }
        if (il.count() > 0) {
            i = il.at(0);
        }
    } else {
        i = filterModel->index(0, 0);
    }
    ui->wordListView->selectionModel()->setCurrentIndex(i, QItemSelectionModel::Clear | QItemSelectionModel::SelectCurrent);
    showDefinition(i);
}

} // namespace PrismaDict
