#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QTimer>
#include <QModelIndex>
#include <QAbstractItemModel>
#include <QTextEdit>
#ifdef USE_WEBKIT
#include <QtWebKit/QWebView>
#endif

namespace Ui {
    class MainWindow;
}

class QSortFilterProxyModel;

namespace PrismaDict {

class WordListModel;
class AboutWidget;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void init();
public slots:
    void loadDict(QString filename);
    void loadStyle(QString styleFilename);
    void reloadStyle();
    void searchIncremental(QString text);
    void searchFullText(QString text);
    void filterList(QString text);
    void showDefinition(QModelIndex index);
    void showFirstDef();
    void onExplSearchButton_Clicked();
    void onActionAbout_triggered();
    void onActionOpen_triggered();
private:
    Ui::MainWindow *ui;
    AboutWidget *aboutDialog;
    QSortFilterProxyModel *filterModel;
    WordListModel *wordListModel;
    QString style;
    QTimer showDefTimer;
#ifdef USE_WEBKIT
    QWebView* webview;
#endif
};

} // namespace PrismaDict

#endif // MAINWINDOW_H
