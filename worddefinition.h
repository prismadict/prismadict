#ifndef WORDDEFINITION_H
#define WORDDEFINITION_H

#include <QtCore/QString>
#include <QtCore/QObject>
#include <QtCore/QMetaType>

namespace PrismaDict {

class WordDefinition
    : public QObject
{
    Q_OBJECT
public:
    static QString convertToHTML(QString xml);
    WordDefinition(QString word, QString explanation);

    QString getPrettyExplanation();

    QString word;
    QString wordNormalized;
    QString explanation;
};

} // namespace PrismaDict

Q_DECLARE_METATYPE(PrismaDict::WordDefinition*)

#endif // WORDDEFINITION_H
