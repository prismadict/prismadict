#-------------------------------------------------
#
# Project created by QtCreator 2011-02-10T23:48:04
#
#-------------------------------------------------
VERSION = 0.1
DEFINES += APP_VERSION=$$VERSION
QT += core gui xmlpatterns widgets
USE_WEBKIT = false
contains(USE_WEBKIT, true) {
  message(webkit enabled)
  QMAKE_CXXFLAGS += -DUSE_WEBKIT
  QT += webkit
} else {
  message(webkit disabled)
}

TARGET = prismadict
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    worddefinition.cpp \
    wordlistmodel.cpp \
    config.cpp \
    util.cpp \
    aboutwidget.cpp

HEADERS  += mainwindow.h \
    worddefinition.h \
    wordlistmodel.h \
    config.h \
    util.h \
    aboutwidget.h

FORMS    += mainwindow.ui \
    aboutwidget.ui

OTHER_FILES += \
    style.css

RESOURCES += \
    prismadict.qrc
