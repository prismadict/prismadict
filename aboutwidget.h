#ifndef ABOUTWIDGET_H
#define ABOUTWIDGET_H

#include <QDialog>

namespace Ui {
    class AboutWidget;
}

namespace PrismaDict {

class AboutWidget : public QDialog
{
    Q_OBJECT

public:
    explicit AboutWidget(QWidget *parent = 0);
    ~AboutWidget();

    void setVersion(QString ver);
private:
    Ui::AboutWidget *ui;
};

} // namespace PrismaDict

#endif // ABOUTWIDGET_H
