#ifndef WORDLISTMODEL_H
#define WORDLISTMODEL_H

#define WORDDEF_ROLE Qt::UserRole
#define FILTER_ROLE Qt::UserRole + 1
#define FILTER_FULLTEXT_ROLE Qt::UserRole + 2

#include <QtCore/QAbstractListModel>
#include <QtCore/QList>
#include <QtCore/QFile>
#include <QtCore/QXmlStreamReader>

namespace PrismaDict {

class WordDefinition;

class WordListModel
    : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit WordListModel(QObject *parent = 0);

    int rowCount ( const QModelIndex & parent = QModelIndex() ) const;
    QVariant data ( const QModelIndex & index, int role = Qt::DisplayRole ) const;

    static WordListModel* loadFromFile(QFile& file);
signals:

public slots:

private:
    static QString readElementTextVerbatim(QXmlStreamReader* reader, QSet<QString>& encounteredElements);
    QList<WordDefinition*> wordList;
    bool fullTextSearch;
};

} // namespace PrismaDict

#endif // WORDLISTMODEL_H
