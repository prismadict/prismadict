#ifndef CONFIG_H
#define CONFIG_H

#include <QString>
#include <QByteArray>

namespace PrismaDict {

class Config
{
public:
    Config();
    virtual ~Config();

private:
    void read();
    void write();

public: // members
    QByteArray windowGeom;
    bool useWebkit;
    QString styleFilename;
    QString xsltFilename;
    quint32 showDefTimeout;
};

extern Config* config;

} // namespace PrismaDict

#endif // CONFIG_H
