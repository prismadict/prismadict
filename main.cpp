#include <QApplication>
#include "mainwindow.h"

using namespace PrismaDict;

#define QUOTE_(x) #x
#define QUOTE(x) QUOTE_(x)

int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("Mattiesworld");
    QCoreApplication::setOrganizationDomain("mattiesworld.gotdns.org");
    QCoreApplication::setApplicationName("PrismaDict");
    QCoreApplication::setApplicationVersion(QUOTE(APP_VERSION));

    QApplication a(argc, argv);
    QStringList args = a.arguments();
    MainWindow w;
    if (args.size() > 1) {
        if (args.size() > 2) {
            w.loadStyle(args.at(2));
        } else {
            w.loadStyle(":/res/style.css");
        }
        w.loadDict(args.at(1));
    }
    w.show();

    return a.exec();
}
