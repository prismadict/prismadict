#include "worddefinition.h"

#include "util.h"

#include <QtDebug>
#include <QtXmlPatterns>

namespace PrismaDict {

QString WordDefinition::convertToHTML(QString xml)
{
    QString buf;

    QXmlStreamReader reader(xml);
    reader.readNext();
    while(!reader.atEnd()) {
        switch(reader.tokenType()) {
        case QXmlStreamReader::StartElement:
            buf += "<div class=\""+reader.name().toString()+"\">";
            break;
        case QXmlStreamReader::EndElement:
            buf += "</div>";
            break;
        case QXmlStreamReader::Characters:
            buf += reader.text();
            break;
        }
        reader.readNext();
    }
    buf += "</div>";
    return buf;
}

WordDefinition::WordDefinition(QString word, QString explanation)
    : QObject(0), word(word), explanation(explanation)
{
    wordNormalized = Util::removeDiacritics(word);
}

QString WordDefinition::getPrettyExplanation()
{
    return convertToHTML("<iosunit>" % explanation % "</iosunit>");

    QFileInfo xsl("prisma.xsl");
    if (!xsl.exists()) return "xsl " + xsl.absoluteFilePath() + " not found";

    qDebug() << "xsl url = " << QUrl::fromLocalFile(xsl.absoluteFilePath());
    qDebug() << explanation;
    QXmlQuery query(QXmlQuery::XSLT20);
    QFile file(xsl.absoluteFilePath());
    file.open(QFile::ReadOnly);
    query.setFocus("<iosunit>" + explanation + "</iosunit>");
//    query.bindVariable("inputDocument", QXmlItem(QVariant("<iosunit>" + explanation + "</iosunit>")));
    query.setQuery(&file, QUrl::fromLocalFile(xsl.absoluteFilePath()));
    if (!query.isValid()) return "invalid query";
    QBuffer outputBuf;
    if (!outputBuf.open(QBuffer::ReadWrite)) return "failed to open buffer";
    QXmlSerializer serializer(query, &outputBuf);
    query.evaluateTo(&serializer);
    qDebug() << QString(outputBuf.data());
    return QString(outputBuf.data());
}

} // namespace PrismaDict

