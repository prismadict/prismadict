#include "config.h"

#include <QSettings>
#include <QtDebug>
#include <QDir>
#include <QCoreApplication>

namespace PrismaDict {

Config* config;

Config::Config()
{
        read();
}

Config::~Config()
{
        write();
}

void Config::read()
{
        qDebug() << "Config::read()";
        QSettings settings;

        windowGeom = settings.value("general/windowGeometry").toByteArray();
        useWebkit = settings.value("style/useWebkit", true).toBool();
        styleFilename = settings.value("style/styleFilename", ":/res/style.css").toString();
        xsltFilename = settings.value("style/xsltFilename", "style.xsl").toString();

        showDefTimeout = settings.value("dict/showDefTimeout", 500).toInt();
}

void Config::write()
{
        qDebug() << "Config::write()";

        QSettings settings;
        settings.setValue("general/version", QCoreApplication::applicationVersion());
        settings.setValue("general/windowGeometry", windowGeom);
        settings.setValue("style/useWebkit", useWebkit);
        settings.setValue("style/styleFilename", styleFilename);
        settings.setValue("style/xsltFilename", xsltFilename);
        settings.setValue("dict/showDefTimeout", showDefTimeout);
}

} // namespace PrismaDict
