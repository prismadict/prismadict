#
# spec file for package [spectemplate]
#
# Copyright (c) 2010 SUSE LINUX Products GmbH, Nuernberg, Germany.
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via http://bugs.opensuse.org/
#

# norootforbuild

Name:          prismadict
Version:       0.1.1
Release:       1
Summary:       Prisma dictionary reader

Group:         Amusements/Teaching/Language
License:       GPL-3.0+ 
URL:           http://mattiesworld.gotdns.org/weblog/category/coding-excursions/prismadict 
Source:        %{name}-%{version}.tar.gz
BuildRoot:     %{_tmppath}/%{name}-%{version}-build
# BuildArch:      noarch

BuildRequires:  libqt5-qtbase-common-devel
BuildRequires:  pkgconfig(Qt5Core) >= 5.5.0
BuildRequires:  pkgconfig(Qt5Gui)
BuildRequires:  pkgconfig(Qt5Widgets)
BuildRequires:  pkgconfig(Qt5XmlPatterns)
#BuildRequires: libQtWebKit-devel 

#Requires: libQtWebKit4

%description
This is the bleeding-edge unstable git version of prismadict, use at your own risk!

%prep
%setup -q


%build
qmake-qt5
make %{?_smp_mflags}


%install
%__install -Dm 0755 %{name} %{buildroot}%{_bindir}/%{name}

%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc
%{_bindir}/%{name}

%changelog
