<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html"/>
	
	<xsl:template match="iosunit">
	<b><xsl:text>test</xsl:text></b>
	<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="ws">
	<xsl:text>test2</xsl:text>
		<b><xsl:value-of select="."/></b><br/>
<!--			<xsl:apply-templates/> -->
	</xsl:template>
	
	<xsl:template match="R">
		<b><xsl:value-of select="."/></b><br/>
	</xsl:template>

	<xsl:template match="btvt">
		<p><xsl:value-of select="."/></p>
	</xsl:template>
		
</xsl:stylesheet>