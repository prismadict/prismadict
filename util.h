#ifndef UTIL_H
#define UTIL_H

class QString;

namespace PrismaDict {

class Util
{
public:
    Util();

    static QString removeDiacritics(QString s);
    static bool containsDiacritic(QString s);
};

} // namespace PrismaDict

#endif // UTIL_H
